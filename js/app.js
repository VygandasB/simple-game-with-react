
class Keturkampis extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      fillOpacity: 1,
      stroke:"black",
      strokeOpacity:1
    }
  }


  _clickedStyle()
  {
    return {
      fillOpacity: 0,
      stroke: "#ff8e1b",
      strokeOpacity: 1
    }
  }


  _notClickedStyle()
  {
    return {
      fillOpacity: 1,
      stroke: "black",
      strokeOpacity: 1
    }
  }

  render(){
    const {isRight, boxId, clickedBoxes, onClickKvadratas} = this.props
    const style = clickedBoxes.includes(boxId) ? this._clickedStyle() : this._notClickedStyle()

    return(
      <svg width="200" height="200">
        <rect data-id={boxId} width="100" height="100" x="50" y="50" style={style} onClick={(e) => onClickKvadratas(e)} />
        {isRight == 1?"":""}
      </svg>
    )
  }
}

class Zaidimas extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      gameName: "Welcome to block game",
      gamePurpose: "Find correct block",
      levels: 3,
      levelName: "First level",
      currentLevel: 1,
      blockAmount: 4,
      maxTry: 4,
      lives: 3,
      livesLabel:"Lives:",
      maxTries:  0,
      clickedBoxes: [],
      failure: false,
      correctBoxNumber: this._random(4),
      winnerMsg: "You win!!!",
      failureMsg:"You lose, refresh and try again"
    }
    this.onClickKvadratas = this.onClickKvadratas.bind(this)
  }


  _maxTries()
  {
    return this.state.maxTry - 1;
  }


  _random(max)
  {
    const randomInt= Math.floor(Math.random() * Math.floor(max));
    return randomInt > 0? randomInt : randomInt + 1;
  }


  onClickKvadratas(e)
  {
    const boxId = e.target.dataset["id"]
    const clickedTry = this.state.maxTries + 1
    const failure = (this._maxTries() == clickedTry) ? true: false
    const updatedLives = this.state.lives - 1

    if(this.state.correctBoxNumber == boxId){
      if(this.state.currentLevel == 1) {
        return this._nextLevel()
      }
      else if (this.state.currentLevel == 2) {
        return this._thirdLevel()
      }
      else if (this.state.currentLevel == 3){
        return this._bonusLevel()
      }
      else if (this.state.currentLevel == 4){
        alert("You won!!!")
      }
    }

      const boxes = this.state.clickedBoxes
      boxes.push(parseInt(boxId))
      this.setState({
        maxTries: clickedTry,
        clickedBoxes: boxes,
        failure: failure,
        lives: updatedLives
      })
  }


  _nextLevel(){
    this.setState({
      currentLevel: 2,
      blockAmount: 8,
      clickedBoxes: 0,
      maxTry: 5,
      levelName: "Second level",
      lives: 4,
      maxTries:  0,
      clickedBoxes: [],
      correctBoxNumber: this._random(8),
      failure: false
    })
    this.onClickKvadratas = this.onClickKvadratas.bind(this)
  }


  _thirdLevel(){
    this.setState({
      currentLevel: 3,
      blockAmount: 12,
      clickedBoxes: 0,
      maxTry: 6,
      levelName: "Third level",
      lives: 5,
      maxTries:  0,
      clickedBoxes: [],
      correctBoxNumber: this._random(12),
      failure: false
    })
    this.onClickKvadratas = this.onClickKvadratas.bind(this)
  }

  _bonusLevel(){
    this.setState({
      levelName: "Bonus level, 1 life find correct box!",
      currentLevel: 4,
      blockAmount: 27,
      lives: 1,
      maxTries:  0,
      maxTry: 2,
      clickedBoxes: [],
      failure: false,
      correctBoxNumber: this._random(27)
    })
    this.onClickKvadratas = this.onClickKvadratas.bind(this)
  }


  _random(max)
  {
    const randomInt= Math.floor(Math.random() * Math.floor(max));
    return randomInt > 0? randomInt : randomInt + 1;
  }


 _boxArray()
 {
   const boxes= []
   for (let i=1; i<=this.state.blockAmount; i++)
   {
     boxes.push(i)
   }
   return boxes
 }


render()
{
  return(
    <div className="container">
      <div><h1>{this.state.gameName}</h1></div>
      <div><p>{this.state.gamePurpose}</p></div>
      <div><p>{this.state.levelName}</p></div>
      <div><p>{this.state.livesLabel}{this.state.lives}</p></div>
      {this.state.failure? this.state.failureMsg: ""}
      {!this.state.failure && this._boxArray().map((v,k) => {
        return (<Keturkampis onClickKvadratas={this.onClickKvadratas} clickedBoxes={this.state.clickedBoxes} key={k} boxId={v} isRight = {this.state.correctBoxNumber == v ? 1: 0} />)
      })
      }
    </div>
  );
}
}

ReactDOM.render(<Zaidimas />, document.getElementById('game'))
