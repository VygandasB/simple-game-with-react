class Keturkampis extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      fillOpacity: 1,
      stroke:"black",
      strokeOpacity:1,
    }
  }


  _clickedStyle()
  {
    return {
      fillOpacity: 0,
      stroke: "#804d00",
      strokeOpacity: 1,

    }
  }


  _notClickedStyle()
  {
    return {
      fillOpacity: 1,
      stroke: "black",
      strokeOpacity: 1
    }
  }

  render(){
    const {isRight, boxId, clickedBoxes, onClickKvadratas} = this.props
    const style = clickedBoxes.includes(boxId) ? this._clickedStyle() : this._notClickedStyle()

    return(
      <svg width="200" height="200">
        <g>
        <rect data-id={boxId} width="100" height="100" x="50" y="50" style={style} onClick={(e) => onClickKvadratas(e)} />
        {isRight == 1?<text x="97" y="102"></text>:""}
        </g>
      </svg>
    )
  }
}


class Zaidimas extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      levels: 3,
      levelName: "First level",
      currentLevel: 1,
      blockAmount: 4,
      maxTry: 3,
      lives: 3,
      maxTries:  3,
      clickedBoxes: [],
      failure: false,
      correctBoxNumber: this._random(4)
    }
    this.onClickKvadratas = this.onClickKvadratas.bind(this)
  }


  _maxTries()
  {
    return this.state.maxTry - 1;
  }


  _random(max)
  {
    const randomInt= Math.floor(Math.random() * Math.floor(max));
    return randomInt > 0? randomInt : randomInt + 1;
  }


  onClickKvadratas(e)
  {
    const boxId = e.target.dataset["id"]
    const clickedTry = this.state.maxTries + 1
    const failure = (this._maxTries() == clickedTry) ? true: false
    const updatedLives = this.state.lives - 1

    if(this.state.correctBoxNumber == boxId){
      if(this.state.currentLevel == 1) {
        return this._nextLevel()
      }
      else if (this.state.currentLevel == 2) {
        return this._thirdLevel()
      }
      else if (this.state.currentLevel == 3) {
        return this._bonusLevel()
      }
      else if (this.state.currentLevel == 4) {
        alert("You win!!!")
      }
    }


      const boxes = this.state.clickedBoxes
      boxes.push(parseInt(boxId))
      this.setState({
        maxTries: clickedTry,
        clickedBoxes: boxes,
        failure: failure,
        lives: updatedLives,
        correctBox: boxId
      })
  }


  _nextLevel(){
    this.setState({
      currentLevel: 2,
      blockAmount: 8,
      clickedBoxes: [],
      maxTry: 6,
      levelName: "Second level",
      lives: 5,
      maxTries:  0,
      correctBoxNumber: this._random(8),
      failure: false
    })
    this.onClickKvadratas = this.onClickKvadratas.bind(this)
  }


  _thirdLevel(){
    this.setState({
      currentLevel: 3,
      blockAmount: 12,
      clickedBoxes: [],
      maxTry: 7,
      levelName: "Third level",
      lives: 6,
      maxTries:  0,
      correctBoxNumber: this._random(12),
      failure: false
    })
    this.onClickKvadratas = this.onClickKvadratas.bind(this)
  }


  _random(max)
  {
    const randomInt= Math.floor(Math.random() * Math.floor(max));
    return randomInt > 0? randomInt : randomInt + 1;
  }


 _boxArray()
 {
   const boxes= []
   for (let i=1; i<=this.state.blockAmount; i++)
   {
     boxes.push(i)
   }
   return boxes
 }


render()
{
  return(
      <div className="container">
      <div><h1>Welcome to block game</h1></div>
      <div><p>Find the correct block</p></div>
      <div><p>{this.state.levelName}</p></div>
      <div><p>Lives: {this.state.lives}</p></div>
      {this.state.failure? <h2>You lose, refresh and try again</h2>: ""}
      {!this.state.failure && this._boxArray().map((v,k) => {
        return (<Keturkampis onClickKvadratas={this.onClickKvadratas} clickedBoxes={this.state.clickedBoxes} key={k} boxId={v} isRight = {this.state.correctBoxNumber == v ? 1: 0} />)
      })
      }
    </div>
  );
}
}

ReactDOM.render(<Zaidimas />, document.getElementById('game'))
